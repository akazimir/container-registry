FROM docker:19.03.12
label maintainer="Anna Kazimirova Kolesarova"
COPY sucet.sh /
COPY test_sucet.sh /
ENTRYPOINT [ "/sucet.sh", "test_sucet.sh" ]
CMD [ "/bin/bash", "/usr/bin/sucet", "/usr.bin/test_sucet" ]
